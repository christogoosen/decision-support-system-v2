@extends('layouts.main')

@section('content')



<div class='alert alert-info'>
    {{Session::get('message')}}
</div>


<div id="container1" class="span5"></div>
<div id="container2" class="span6"></div>
<div id="container3" class="span7"></div>	


<script type="text/javascript">
$(function() {
  $('#container1').highcharts(
    {{json_encode($graph1JSON)}}
  )
});
$(function() {
  $('#container2').highcharts(
    {{json_encode($graph2JSON)}}
  )
});
$(function() {
  $('#container3').highcharts(
    {{json_encode($graph3JSON)}}
  )
});
</script>



@stop
