<html>

<head>

<title>Point8</title>
<link rel="stylesheet"  href="{{URL::asset('css/bootstrap.css')}}">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script> 
<script src="/js/highcharts.js" type="text/javascript"></script>
<script src="/js/highcharts-more.js" type="text/javascript"></script>
</head>

<body class="backboard">
	<div class="container-invis"></div>
	<div class="container-main">

		@yield('content')

	</div>
</body>

</html>