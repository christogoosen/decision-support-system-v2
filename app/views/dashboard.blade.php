@extends('layouts.main')

@section('content')


<div class='container-weight'>
{{ Form::open(array('url'=>'weights')) }}
<div class="row">
<div class="col-md-4">
{{ Form::label('first','First Year Weight:',array('id'=>'','class'=>'')) }}
{{ Form::number('first','',array('id'=>'','class'=>'weightings')) }}
</div>

<div class="col-md-4">
{{ Form::label('second','Second Year Weight:',array('id'=>'','class'=>'')) }}
{{ Form::number('second','',array('id'=>'','class'=>'weightings')) }}
</div>

<div class="col-md-4">
{{ Form::label('third','Third Year Weight:',array('id'=>'','class'=>'')) }}
{{ Form::number('third','',array('id'=>'','class'=>'weightings')) }}
</div>
</div>
<br>
{{ Form::submit('Save', ['class' => 'btn btn-info btn-lg btn-block width' ]) }}

{{ Form::close() }}
</div>

<br>

<div id="container1" class="span5 container-scat"></div>
<br>
<br>
<div id="container2" class="span6 container-scat"></div>
<br>
<br>
<div id="container3" class="span7 container-scat"></div>   


<script type="text/javascript">
$(function() {
  $('#container1').highcharts(
    {{json_encode($graph1JSON)}}
  )
});
$(function() {
  $('#container2').highcharts(
    {{json_encode($graph2JSON)}}
  )
});
$(function() {
  $('#container3').highcharts(
    {{json_encode($graph3JSON)}}
  )
});
</script>





@stop
