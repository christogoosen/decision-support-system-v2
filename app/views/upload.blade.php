@extends('layouts.main')

@section('content')
{{FileController::wipeCache()}}
{{DatabaseController::clearDatabase()}}

@if(Session::has('message'))
<div class='alert alert-danger'>
	{{Session::get('message')}}
</div>
@endif

<div class="container-logo"><image src="images/logo7.png" height="400" width="400"></div>

<div class='container-inner'>
{{ Form::open(array('url'=>'dashboard','files'=>true)) }}
  
  {{ Form::label('file','Upload Your File:',array('id'=>'','class'=>'')) }}
  {{ Form::file('file','',array('id'=>'','class'=>'')) }}

<br>
  <!-- submit buttons -->
  {{ Form::submit('Save', ['class' => 'btn btn-info' ]) }}
  
  <!-- reset buttons -->
  {{ Form::reset('Reset', ['class' => 'btn btn-info' ]) }}

  {{ Form::close() }}

</div>

@stop