<?php


//Route::get('/dss', array('as' => 'upload', 'uses' => 'HomeController@fileupload'));

Route::get('/', array('as' => 'upload', 'uses' => 'HomeController@getUpload'));

Route::get('/dashboard', array('as' => 'upload', 'uses' => 'GraphController@Allgraphs'));

Route::post('/weights', array('uses'=>'GraphController@Allgraphs'));

//Route::get('/dashboard', function(){
//	return View::make('dashboard');
//});



Route::post('dashboard', function(){
FileController::wipeCache();
DatabaseController::clearDatabase();
if(Input::hasFile('file')) {
$file =Input::file('file');
$upload_success = Input::file('file')->move('uploads',Input::file('file')->getClientOriginalName());
}
else{Session::flash('message', 'No file has been specified');
	return View::make('upload');}


if( $upload_success ) {	
return FileController::unzip($upload_success);
$upload_success=[];
//return Route::get('uploadSuccessfull', array('as' => 'uploadSuccess', 'uses' => 'FileController'));
//return Response::json('success', 200);
} 
else {
   return Response::json('error', 400);
}

});

