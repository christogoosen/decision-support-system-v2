<?php

class GraphController extends BaseController {

	public static function Allgraphs()
	{
	if(Input::get("first") && Input::get("second") && Input::get("third"))
    {
		 $weight1 = (float)(Input::get("first"))/10;
    	 $weight2 = (float)(Input::get("second"))/10;
    	 $weight3 = (float)(Input::get("third"))/10;
    }
    else
    { 
    	 $weight1 = 0.1;
    	 $weight2 = 0.2;
    	 $weight3 = 0.7;
    }
		 $graph1JSON = GraphController::graph1($weight1,$weight2,$weight3);  
		 $graph2JSON = GraphController::graph2();
		 $graph3JSON = GraphController::graph3();
		 return View::make('dashboard')->with(array("graph1JSON"=>$graph1JSON, "graph2JSON"=>$graph2JSON, "graph3JSON"=>$graph3JSON));
	}

	public static function graph1($weight1,$weight2,$weight3)
	{
		$testData = DatabaseController::graph1($weight1,$weight2,$weight3);
		$Above60Array = array();
		$Below60Array = array();
		$count =0;
		foreach ($testData as $key ) 
		{
		
		foreach ($key as $student) 
		{ 
  			//$categoryArray[] = $student[0]; 
  			
  			//don't show users who have no assets
  			if($count<1){

  			array_push($Above60Array,array("x"=>(float)$student[2],"y"=>(float)$student[1],"z"=>$student[0]));
  			}
  			else
  			{
  				array_push($Below60Array,array("x"=>(float)$student[2],"y"=>(float)$student[1],"z"=>$student[0]));
  			}//var_dump($chartArray["series"]);
  			//var_dump(json_encode(array(array((float)$student[1],(float)$student[2]))));
		} 
		//var_dump($chartArray["series"]["data"]);
		
		$count +=1;
  			if($count>0)
  			{
  				$seriesName="Below 60"; 
  			}

		}
		$chartArray["chart"]["type"] = "scatter";
		$chartArray["chart"]["zoomType"] = "xy";

		$chartArray["title"]["text"] = 'Technical Average vs Weighted Average';

		$chartArray["credits"]["enabled"] = false;

		$chartArray["xAxis"]["title"]["enabled"] = true;
		$chartArray["xAxis"]["title"]["text"] = "Technical Average";
		$chartArray["xAxis"]["startOnTick"] = true;
		$chartArray["xAxis"]["endOnTick"] = true;
		$chartArray["xAxis"]["showLastLabel"] =true;
		$chartArray["xAxis"]["max"] = 100;
		$chartArray["xAxis"]["min"] = 50;

		$chartArray["yAxis"]["max"] = 100;
		$chartArray["yAxis"]["title"]["text"] = "Weighted Average";
		$chartArray["xAxis"]["min"] = 50;

		$chartArray["legend"]["layout"] = 'vertical';
		$chartArray["legend"]["align"] = 'left';
		$chartArray["legend"]["verticalAlign"] = 'top';
		$chartArray["legend"]["x"] = 100;
		$chartArray["legend"]["y"] = 70;
		$chartArray["legend"]["floating"] = true;
		$chartArray["legend"]["backgroundColor"] = '#FFFFFF';
		$chartArray["legend"]["borderWidth"] =1;

		$chartArray["plotOptions"]["scatter"]["marker"]["radius"] = 5;
		$chartArray["plotOptions"]["scatter"]["marker"]["states"]["hover"]["enabled"]=true;
		$chartArray["plotOptions"]["scatter"]["marker"]["states"]["hover"]["lineColor"] = 'rgb(100,100,100)';
		$chartArray["plotOptions"]["scatter"]["states"]["hover"]["marker"]["enabled"] = false;

		$chartArray["plotOptions"]["scatter"]["tooltip"]["headerFormat"] = '<b>{series.name}</b><br>';
		$chartArray["plotOptions"]["scatter"]["tooltip"]["pointFormat"] = '{point.z} <br>x: {point.x}%, y: {point.y}%';

		$chartArray["series"][0]["name"]='Above 60';
		$chartArray["series"][0]["color"]='rgba(204, 0, 0, 1)'; 
		$chartArray["series"][0]["data"]=$Above60Array;

		$chartArray["series"][1]["name"]='Below 60';
		$chartArray["series"][1]["color"]='rgba(0, 0, 102, 1)'; 
		$chartArray["series"][1]["data"]=$Below60Array;

	return $chartArray;
	}

	public static function graph2()
	{
		$markArray =[];
		$testData = DatabaseController::graph2();
		foreach ($testData as $student) {

			array_push($markArray, array("x"=>$student[1],"y"=>$student[2],"z" =>$student[0]));
		}
		//var_dump($markArray);
		$chartArray["chart"]["type"] = "scatter";
		$chartArray["chart"]["zoomType"] = "xy";

		$chartArray["title"]["text"] = 'Technical Module Average vs "Softer" Sciences Module Average';

		$chartArray["credits"]["enabled"] = false;

		$chartArray["xAxis"]["title"]["enabled"] = true;
		$chartArray["xAxis"]["title"]["text"] = "Technical Module Average";
		$chartArray["xAxis"]["startOnTick"] = true;
		$chartArray["xAxis"]["endOnTick"] = true;
		$chartArray["xAxis"]["showLastLabel"] =true;
		$chartArray["xAxis"]["max"] = 100;
		$chartArray["xAxis"]["min"] = 50;

		$chartArray["yAxis"]["max"] = 100;
		$chartArray["yAxis"]["title"]["text"] = "''Softer'' Sciences Module Average";
		$chartArray["xAxis"]["min"] = 50;

		$chartArray["legend"]["layout"] = 'vertical';
		$chartArray["legend"]["align"] = 'left';
		$chartArray["legend"]["verticalAlign"] = 'top';
		$chartArray["legend"]["x"] = 100;
		$chartArray["legend"]["y"] = 70;
		//$chartArray["legend"]["z"] = 70;
		$chartArray["legend"]["floating"] = true;
		$chartArray["legend"]["backgroundColor"] = '#FFFFFF';
		$chartArray["legend"]["borderWidth"] =1;

		$chartArray["plotOptions"]["scatter"]["marker"]["radius"] = 5;
		$chartArray["plotOptions"]["scatter"]["marker"]["states"]["hover"]["enabled"]=true;
		$chartArray["plotOptions"]["scatter"]["marker"]["states"]["hover"]["lineColor"] = 'rgb(100,100,100)';
		$chartArray["plotOptions"]["scatter"]["states"]["hover"]["marker"]["enabled"] = false;

		$chartArray["plotOptions"]["scatter"]["tooltip"]["headerFormat"] = '';

		$chartArray["plotOptions"]["scatter"]["tooltip"]["pointFormat"] = '<b>{point.z}</b> <br>x: {point.x}%, y: {point.y}%';

		$chartArray["series"][0]["name"]='Students';
		$chartArray["series"][0]["color"]='rgba(204, 0, 0, 1)'; 
		$chartArray["series"][0]["data"]=$markArray;
		//var_dump(json_encode($chartArray));

	return $chartArray;
	}

	public static function graph3()
	{
		$firstArr =[];
		$secondArr =[];
		$thirdArr =[];
		$testData = DatabaseController::graph3();
		$chartArray["chart"] = array("type" => "column"); 
		$chartArray["title"] = array("text" => "Module Averages over three years"); 
		$chartArray["credits"] = array("enabled" => false); 
		$chartArray["navigation"] = array("buttonOptions" => array("align" => "right")); 

		$chartArray["tooltip"]["headerFormat"] ='<span style="font-size:10px">{point.key}</span><table>';
		$chartArray["tooltip"]["pointFormat"] = '<tr><td style="color:{series.color};padding:2">{series.name}: </td>'.'<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>';
        $chartArray["tooltip"]["footerFormat"] = '</table>';
        //$chartArray["tooltip"]["borderRadius"] =6;
        $chartArray["tooltip"]["shared"] = true;
        $chartArray["tooltip"]["useHTML"] = true;   
        $chartArray["plotOptions"]["column"]["pointPadding"] =0.2;
        $chartArray["plotOptions"]["column"]["borderWidth"] =0;


		$chartArray["series"] = array(); 
		$chartArray["xAxis"] = array("categories" => array()); 


		foreach ($testData as $student) 
		{ 
  			$categoryArray[] = $student[0]; 
  			array_push($firstArr, (float)$student[1]);
			array_push($secondArr, (float)$student[2]);
			array_push($thirdArr, (float)$student[3]);
  			//don't show users who have no assets
  					//var_dump($chartArray["series"]);
		} 
		$chartArray["series"][] = array("name" => "1st Year", "data" => $firstArr); 
		$chartArray["series"][] = array("name" => "2nd Year", "data" => $secondArr); 
		$chartArray["series"][] = array("name" => "3rd Year", "data" => $thirdArr); 
  	
		//var_dump($testData[3]);
		$chartArray["xAxis"] = array("categories" => $categoryArray); 
		$chartArray["yAxis"] = array("title" => array("text" => "Year Averages")); 
		$chartArray["yAxis"]["min"] = 50;
		$chartArray["yxis"]["max"] = 100;
	

		return $chartArray;
	}
	public static function dataTest()
	{
		$testData = DatabaseController::graph1();
		$Above60Array = array();
		$Below60Array = array();
		$count =0;
		foreach ($testData as $key ) 
		{
		
		foreach ($key as $student) 
		{ 
  			if($count==1){

  			array_push($Above60Array,array(((float)$student[1]),((float)$student[2])));
  			}
  			else
  			{
  				array_push($Below60Array,array(((float)$student[1]),((float)$student[2])));
  			}
		} 

			$count +=1;
  			if($count>0)
  			{
  				$seriesName="Below 60"; 
  			}

		}
		return $chartArray;
	}
}