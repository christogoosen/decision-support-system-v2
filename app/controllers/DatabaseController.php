<?php

class DatabaseController extends BaseController {

  

	public static function clearDatabase()
	{
		
		DB::table('applicants')->delete();
                DB::table('statistics')->delete();
	}

	public static function insertData($variable)
	{

                foreach ($variable as $key) {
                       

                /*$stats = new Statistic;
                $stats->module = ;
                $stats->year = ;
                $stats->moduleAverage = ;
                $stats->applicantAverage = ;
                $stats->standardDeviation = ;
                $stats->save();*/

		            $applicant = new Applicant;
                $applicant->studentNumber = $key["studentNumber"];
                $applicant->m101 = $key["m101"];
                $applicant->m102 = $key["m102"];
                $applicant->m103 = $key["m103"];
                $applicant->firstYearAverage = $key["AvgYear1"];
                $applicant->m201 = $key["m201"];
                $applicant->m202 = $key["m202"];
                $applicant->m203 = $key["m203"];
                $applicant->secondYearAverage = $key["AvgYear2"];
                $applicant->m301 = $key["m301"];
                $applicant->m302 = $key["m302"];
                $applicant->m303 = $key["m303"];
                $applicant->thirdYearAverage = $key["AvgYear3"];
                $applicant->save();
              }
              DatabaseController::filterData();
	}


        public static function filterData()
        {
                //Getting a list of all studentNumbers in the applicants table
                $all = DB::table('applicants')->lists('studentNumber');
                $count =0;
                //identifying the studentNumbers that have less than 50 for a module
                foreach ($all as $key) {
                      $results = DB::table('applicants')
                      ->where('m101', '<', 50)
                      ->orwhere('m102', '<', 50)
                      ->orwhere('m103', '<', 50)
                      ->orwhere('m201', '<', 50)
                      ->orwhere('m202', '<', 50)
                      ->orwhere('m203', '<', 50)
                      ->orwhere('m301', '<', 50)
                      ->orwhere('m302', '<', 50)
                      ->orwhere('m303', '<', 50)
                      ->lists('studentNumber'); 
                      $count +=1;
                     
                }
                 //var_dump('detected: '.$count);
                //deleting the identified student numbers with less than 50 for a module
                foreach ($results as $value) {
                    DB::table('applicants')->where('studentNumber', $value)->delete();
                }
                
               // var_dump('After database: '.count($all = DB::table('applicants')->lists('studentNumber')));
        }


        //Quesries for the Technical Average vs. the Weighted Average Graph
        public static function graph1($weight1,$weight2,$weight3)
        {

          //selecting the studentNumbers that have above 60 for all modules 
          $above60 = DB::table('applicants')
          ->where('m103', '>', 60)
          ->where('m203', '>', 60)
          ->where('m303', '>', 60)
          ->get();

          //var_dump($above60);

          //selecting the studentNumbers that have below 60 for at least one module
          $below60 = DB::table('applicants')
          ->where('m103', '<', 60)
          ->orwhere('m203', '<', 60)
          ->orwhere('m303', '<', 60)
          ->get();
          //->lists('studentNumber');

          #var_dump($below60);
          $loop60s = array($above60, $below60);
          $finalArray = array();
          foreach ($loop60s as $all) 
          {
                   
          //selecting and calculating the average for each student
          $allArray=[];
          foreach ($all as $student ) {
                    $moduleAve = round((float)((($student->m103) + ($student->m203) + ($student->m303))/3), 1);
                    $first = $student->firstYearAverage;
                    $second = $student->secondYearAverage;
                    $third = $student->thirdYearAverage;
                    $singleAve = round((float)(($first*$weight1) + ($second*$weight2) + ($third*$weight3)), 1);

                    array_push($allArray, array(($student->studentNumber),$singleAve, $moduleAve));

          }
          
          //var_dump($allArray);
          
          array_push($finalArray, $allArray);
          //The weighted average for student - high weighting to 3rd year
          }
          //return $allArray;
          //var_dump($finalArray[1]);
          return $finalArray;
          
        }    

        public static function graph2()
          {
             //selecting and calculating the average for each student
                    $all = DB::table('applicants')->get();
                    $allArray= array();
                    foreach ($all as $student) 
                    {
                    $moduleAve = round((float)((($student->m103) + ($student->m203) + ($student->m303))/3), 1);
                    $softAve = round((float)((($student->m101) + ($student->m102) + ($student->m201) + ($student->m202) + ($student->m301) + ($student->m302))/6), 1);
                    array_push($allArray, array(($student->studentNumber),$moduleAve,$softAve));
                    }


                    return $allArray;
                  //var_dump($allArray);
          }

          public static function graph3()
          {
                    $all = DB::table('applicants')->get();
                    $allArray=[];
                    foreach ($all as $average) {
                    $first = $average->firstYearAverage;
                    $second = $average->secondYearAverage;
                    $third = $average->thirdYearAverage;
                    array_push($allArray, $modArr=[($average->studentNumber), $first, $second, $third]);

                    }

                    //var_dump($allArray[3]);
                    return $allArray;
          }
          public static function Allgraphs()
          {
              $graph1Arr = DatabaseController::graph1();
              $graph2Arr = DatabaseController::graph2();
              $graph3Arr = DatabaseController::graph3();

          }



}