<?php

class FileController extends BaseController {

	public static function wipeCache()
	{
		$files = glob("uploads/*");
		foreach($files as $file)
		{
			if(is_file($file))
			{
				unlink($file);
			}
			elseif(is_dir($file) )
			{
				if($file != '*MACOSX')
				{
				$dirFiles = glob($file.'/*');
				foreach ($dirFiles as $dirFile) {
					if(is_file($dirFile))
					{
					unlink($dirFile);
					}
				}
				//rmdir($file);
				//Session::flash('message',$file.'/*');
			}
			}
		}
		
	}

	public static function unzip($file)
	{
		$files = glob("uploads/*.zip");
		if($file = $files[0])
		{
		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
  			$zip->extractTo('uploads');
  			$zip->close();
  			//echo 'woot!';
  			//exec('sudo rm '.$file.' echo "nepenwyh&6587"');
  			unlink($file);
  			//rmdir('__MACOSX');
  			//delete($file);
  	
  			FileController::readFiles($file);
  			return GraphController::Allgraphs();
		} else {
  		//echo 'doh!';
			return View::make('upload');
			Session::flash('message','Unzip error');
		}
		//return View::make('upload');
	}
	else
	{return View::make('upload');}
	   
	}

	public static function readFiles($dir)
	{
		$directory = basename($dir,'.zip');
		//var_dump($directory);
		$files = glob('uploads/'.$directory.'/*.csv');
		$studentData = [];
		$applicantsStudentNum =[];
		$applicantsFile = $files[array_search('uploads/'.$directory.'/applicants.csv', $files)];

		$fileApplicants = fopen($applicantsFile, 'r');
		while(($appData = fgetcsv($fileApplicants,'1000',',')) !==FALSE)
		{
			if(in_array('student', $appData)==FALSE)
			{
				array_push($applicantsStudentNum, $appData[0]);
			}
		}
		fclose($fileApplicants);


		foreach ($files as $file) {
			
			$fileFull = basename($file, 'uploads/');
			$fileFull = basename($fileFull,'.csv');
			if($fileFull != 'applicants'){
			$fileFull = explode('_', $fileFull);
			$module =$fileFull[0];
			$year =$fileFull[1];
			//var_dump($fileFull);
			}

			if($file !=$applicantsFile)
			{

			if(($f = fopen($file,'r')) !== FALSE)
			{
				while(($data = fgetcsv($f,'1000',',')) !==FALSE)
				{
					//var_dump($data[0]);
					if((in_array('student',$data)==FALSE) )
					{
						if(in_array($data[0], $applicantsStudentNum))
							{
								array_push($data, $module);
								array_push($data, $year);
								array_push($studentData, $data);
							}
					}
				}	
			}
		}
			
		}
		//var_dump($applicantsFile);
		//var_dump($applicantsStudentNum);
		//var_dump(count($applicantsStudentNum));
		//var_dump($studentData);

		FileController::applicantDictionary($applicantsStudentNum, $studentData);

		//var_dump($files);
	}
	public static function applicantDictionary($applicantArray, $studentData)
	{
		$studentAmount = count($applicantArray)+1;
		$applicationDict = [];
		foreach ($applicantArray as $student) 
		{
			//var_dump($student);
			$studentDict =[
			"studentNumber" => $student,
			"m101" => 0,
			"m102" => 0,
			"m103" => 0,
			"AvgYear1" => 0.00,
			"m201" => 0,
			"m202" => 0,
			"m203" => 0,
			"AvgYear2" => 0.00,
			"m301" => 0,
			"m302" => 0,
			"m303" => 0,
			"AvgYear3" => 0.00,
			"OverAllAvg" => 0.00
			];
			array_push($applicationDict, $studentDict);
		}
		//var_dump($studentData[1]);
		FileController::setApplicationDictionary($applicationDict, $studentData);
		//var_dump($applicationDict[0]);
	}

	public static function setApplicationDictionary($applicationDict, $studentData)
	{
		$finalDict = [];
		foreach ($applicationDict as $student) 
		{
			//var_dump($student["studentNumber"]);
			foreach ($studentData as $data) 
			{
				//var_dump($student);
				if($data[0]==$student['studentNumber'])
				{
					//var_dump($student);
					if ($data[2]=="101") 
					{
						$student["m101"] = (float)$data[1];	
					}
					elseif ($data[2]=="102") {
						$student["m102"] = (float)$data[1];
					}
					elseif ($data[2]=="103") {
						$student["m103"] = (float)$data[1];
					}
					elseif ($data[2]=="201") {
						$student["m201"] = (float)$data[1];
					}
					elseif ($data[2]=="202") {
						$student["m202"] = (float)$data[1];
					}
					elseif ($data[2]=="203") {
						$student["m203"] = (float)$data[1];
					}
					elseif ($data[2]=="301") {
						$student["m301"] = (float)$data[1];
					}
					elseif ($data[2]=="302") {
						$student["m302"] = (float)$data[1];
					}
					elseif ($data[2]=="303") {
						$student["m303"] = (float)$data[1];

					}
				}
				
				
			}
			$student["AvgYear1"] = (float)(($student["m101"] + $student["m102"] + $student["m103"])/3);
			$student["AvgYear2"] = (float)(($student["m201"] + $student["m202"] + $student["m203"])/3);
			$student["AvgYear3"] = (float)(($student["m301"] + $student["m302"] + $student["m303"])/3);
			$student["OverAllAvg"] = (float)(($student["AvgYear1"] + $student["AvgYear2"] + $student["AvgYear3"])/3);
			array_push($finalDict, $student);
			
		}
		//var_dump($finalDict[2]);
		//var_dump('Before database: '.count($finalDict));
		DatabaseController::insertData($finalDict);
		
	}

}
