<?php

class Applicant extends Eloquent {

	protected $table = 'applicants';
	protected $fillable = array('studentNumber', '101', '102', '103', 'firstYearAverage', '201', '202',
		'203', 'secondYearAverage', '301', '302', '303', 'thirdYearAverage');
        	
}